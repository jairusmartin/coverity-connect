#!/bin/bash
pwd=$(dirname $(readlink -f $0))
wkdir=$(realpath $pwd/..)

echo
pushd $wkdir
docker-compose exec coverity cov-im-ctl status
popd
