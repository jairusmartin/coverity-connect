sudo apt-get update && sudo apt-get upgrade -y

if hash docker 2>/dev/null; then
    echo "Docker is already installed...skipping."
else
    curl -sSL https://get.docker.com | sh
fi

sudo usermod -aG docker ${USER}

sudo apt-get install libffi-dev libssl-dev
sudo apt install python3-dev
sudo apt-get install -y python3 python3-pip
sudo pip3 install docker-compose

sudo systemctl enable docker

echo

docker -v
docker-compose -v

echo "Exit shell and log back for user group Docker to take effect."
