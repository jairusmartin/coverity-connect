# environment setup
Create 'projects' directory in your home folder:

    `mkdir ~/projects`

Clone Home-Assistant to ~/project:

    `git clone git@gitlab.com:jairusmartin/coverity-connect.git ~/projects`

# service commands

Navigate to project directory:

    `cd ~/projects/coverity-connect`

Install Docker and Docker-Compose:

    `./bin/setup.sh`

Configure ngix.conf and .env:

    1. configure etc/nginx/conf.d/some.site.com.conf
       Replace instances of `some.site.com` with your domain.
    1. copy `.env-dist` to `.env` 
       Note: docker uses .env variables for the build and deploy.
    1. Update .env with the license file and install path names that were placed in the build folder


Fetch installer and license, place in build folder and rename (see placeholders)

     `build/license.dat`
     `build/installer.sh`

Bring service up:

    `./bin/up.sh`

Bring service down:

    `./bin/up.sh`

###Document TODOs:
1. TBD Run gethostname inside of docker container.
1. Navitgate to [Synopsys Community](https://community.synopsys.com/) and run the reshost script inside the docker container.



