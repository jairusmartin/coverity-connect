#!/usr/bin/env bash
# docker_entrypoint.sh

# Start Coverity Platform service
/home/coverity/cov-platform/bin/cov-im-ctl start

# Tail Coverity Platform log
tail -F /home/coverity/cov-platform/logs/cim.log

